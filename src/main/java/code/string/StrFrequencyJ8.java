package code.string;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StrFrequencyJ8
{
    // Not considering the order of characters.
    static Map<Character,Integer> strFrequency(String st)
    {
//        Stream.of(st.toLowerCase().split(""))
//                .collect(Collectors.groupingBy(s->s,Collectors.counting()))
//                .entrySet()
//                .forEach(System.out::print);

        // OR
    Stream.of(st.split(""))
            .collect(Collectors.groupingBy(s->s,Collectors.counting()))
            .entrySet()
            .stream()
            .map(s-> s.getKey()+s.getValue())
            .forEach(System.out::print);
        return null;
    }

    //Character frequency in the order of their occurrence(LinkedHashMap::new).
    static void stringFreq(String st)
    {
        Stream.of(st.toCharArray())
                .collect(Collectors.groupingBy(e -> e, LinkedHashMap::new, Collectors.counting()))
                .entrySet()
                .stream()
                .forEach(System.out::print);
    }
    public static void main(String[] args)
    {
        System.out.println("Program to find the string frequency,");
        strFrequency("Java");
    }
}
