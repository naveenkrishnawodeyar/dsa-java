package code.string;

import java.util.Scanner;

public class PalindromePgm
{
    private static void palindromeCheck(String st)
    {
        String[] str = st.toLowerCase().split("");
        String rev = "";

        for(int i=str.length-1; i>=0; i--)
        {
            rev +=str[i];
        }
        System.out.println(st);
        System.out.println(rev);

        if(st.contentEquals(rev))
            System.out.println("Given string is palindrome");
        else
            System.out.println("Given string isn't palindrome");
    }


    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter an string to check for palindromes:");
        String st = sc.nextLine();
        palindromeCheck(st);
    }
}
