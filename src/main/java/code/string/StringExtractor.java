// Write an java program to extract separate the characters, digits and special characters from an string.,
public class StringExtractor
{
	public static void main(String[] args)
	{
	
		String st = "Naveen123@Kumar#465";
			
		StringBuilder characters = new StringBuilder();
		StringBuilder numbers = new StringBuilder();
		StringBuilder symbols = new StringBuilder();

			for(int i=0; i< st.length(); i++)
			{
				if(Character.isAlphabetic(st.charAt(i)))
				{
					characters.append(st.charAt(i));
				}
				
				else if (Character.isDigit(st.charAt(i)))
				{
					numbers.append(st.charAt(i));	
				}
				
				else
					symbols.append(st.charAt(i));
			}
			System.out.println("Alphabets in the string, "+characters);
			System.out.println("Numbers in the string, "+numbers);
			System.out.println("Symbols in the string, "+symbols);
	}
}