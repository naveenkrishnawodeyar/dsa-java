// Write an java program to print 0-100 usin an for loop and print evenNumbers and oddNumbers seperately.,

public class EvenOddNumbersExtracter
{
	public static void main(String[] args)
	{
			StringBuilder evenNumbers = new StringBuilder();
			StringBuilder oddNumbers = new StringBuilder();
			
				for(int i=0; i<=100; i++)
				{
					if(i%2==0)
					{
							evenNumbers.append(" "+i);
					}
					else
					{
						oddNumbers.append(" "+i);
					}
				}
				
				System.out.println("Even Numbers, "+evenNumbers);
				System.out.println("Odd Numbers,"+oddNumbers);
	}
}