package code.string;

import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StrFrequency1
{
    // Print characters frequency in alphabetic order.
    static void strFrequency(String st)
    {
        Stream.of(st.toLowerCase().split(""))
                .collect(Collectors.groupingBy(s->s,Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(System.out::print);
    }

    // Print characters frequency in the order of most frequent one to the least frequent one.
    static void stFrequency(String st)
    {
        Stream.of(st.toLowerCase().split(""))
                .collect(Collectors.groupingBy(s->s, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue((a,b)->b.compareTo(a)))
                .forEach(System.out::print);
    }

    public static void main(String[] args)
    {
        System.out.println("Program to find the character frequency,");
        strFrequency(new String("Madam"));
        System.out.println("\n*****************\n");
        stFrequency("RaceCar");
    }
}
