
public class StringTemplate{
	public static void main(String args[]){
		System.out.println("JDK 21 reached General Availability on 19 September 2023");
	
		int a = 1;
		int b = 2;
		String st = "String Template";
		
		System.out.println(STR."value of a: \{a}");
		System.out.println(STR."value of b:\{b}");
		System.out.println(STR."value of st:\{st}");
	}
}