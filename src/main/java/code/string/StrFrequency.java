package code.string;

import java.util.Map;
import java.util.TreeMap;

public class StrFrequency
{
    static void strFrequency(String st)
    {
        Map<Character,Integer> treeMap = new TreeMap<>();

        for(char c: st.toLowerCase().toCharArray())
        {
            if(treeMap.containsKey(c))
                treeMap.put(c,treeMap.get(c)+1);
            else
                treeMap.put(c,1);
        }
        System.out.println(treeMap);

    }

    public static void main(String[] args)
    {
        System.out.println("Program to find the string frequency,");
        strFrequency(new String("Madam"));
    }
}
