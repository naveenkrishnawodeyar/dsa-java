package code.string;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StrFrequencyJ8Ex
{
    // Not considering the order of characters.
    static Map<Character,Integer> strFrequency(String st)
    {
        Stream.of(st.toLowerCase().split(""))
                .collect(Collectors.groupingBy(c->c, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted()
                .map(s-> s.getKey()+s.getValue())
                .forEachOrdered(System.out::print);

        return null;
    }

    //Character frequency in the order of their occurrence(LinkedHashMap::new).
    static void stringFreq(String st)
    {
        Stream.of(st.toLowerCase().split(""))
                .collect(Collectors.groupingBy(s->s,LinkedHashMap::new,Collectors.counting()))
                .entrySet()
                .stream()
                .sorted()
                .forEach(System.out::print);
    }
    public static void main(String[] args)
    {
        System.out.println("Program to find the string frequency,");
        strFrequency("\nJava\n");
        strFrequency("Interface\n");
    }
}
