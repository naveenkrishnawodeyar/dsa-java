// Write an java program to seperate the content in an give String;

public class StringExtractor2
{
	public static void main(String args[])
	{
			String st = "Naveen@123$Kumar K#456";
			StringBuilder characters = new StringBuilder();
			StringBuilder numbers = new StringBuilder();
			StringBuilder symbols = new StringBuilder();

				for(char c: st.toCharArray())
				{
					if(Character.isAlphabetic(c))
					{
						characters.append(c);
					}
					else if(Character.isDigit(c))
					{
						numbers.append(c);
					}
					else
						symbols.append(c);
				}
				
				System.out.println("Characters in the given string,"+characters);
				System.out.println("Numbers in the given string,"+numbers);
				System.out.println("Special Characters in the given string,"+symbols);
	}
}