package code.string;

import java.util.Scanner;

public class PalindromePgm2
{
    private static void palindromeCheck(String st)
    {
       String str = st.toLowerCase();

       for (int i=0; i<=str.length()/2; i++)
       {
           if(str.charAt(i) == str.charAt(str.length()-i-1))
               System.out.println("Given string,"+st+" is an Palindrome");
           else
               System.out.println("Given string,"+st+" isn't an Palindrome");
       }
    }


    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter an string to check for palindromeness:");
        String st = sc.nextLine();
        palindromeCheck(st);
        palindromeCheck("level");
    }
}
