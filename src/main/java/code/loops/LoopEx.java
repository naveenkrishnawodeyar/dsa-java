package code.loops;

public class LoopEx
{
    public static void main(String[] args)
    {
        System.out.println("Loop is a control structure that allows you to repeatedly execute a block of code\n" +
                " as long as a specified condition is true or for a fixed number of iterations");
    }

    static
    {
        int i=0;
        // condition
        while(i<=100)
        {
            // code to be executed,
            System.out.println(i+",");

            // increment/decrement,
            i +=1;
        }
    }
}
