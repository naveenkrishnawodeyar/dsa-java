package code.loops;

public class DoWhileEx
{
    public static void main(String[] args)
    {
        System.out.println("do-while loop is used to iterate a part of the program repeatedly,\n" +
                " until the specified condition is true.");
    }
    static
    {
        int i=0;
        do
        {
            System.out.println("Mandatory Statement,");
            i++;
        }
        while(i<=0);
    }
}
