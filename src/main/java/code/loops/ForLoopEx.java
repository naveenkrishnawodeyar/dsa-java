package code.loops;

public class ForLoopEx
{
    public static void main(String[] args)
    {
        System.out.println("If the number of iteration is fixed, it is recommended to use for loop.");
    }

    static
    {
        for (int i = 0; i <= 100; i++)
        {
            System.out.println(i);

            if(i / 2 ==0)
                continue;
        }
    }
}
