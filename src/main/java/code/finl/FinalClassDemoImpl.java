package code.finl;

import java.util.Scanner;

public  class FinalClassDemoImpl //extends FinalClassDemo
{
    public static void main(String[] args)
    {
        System.out.println("Can't extend the final class\n");
        var obj = new FinalClassDemo();
        System.out.println(obj.getClsId());
        System.out.println(obj.getClsName());
        System.out.println(obj.getClsValue());
        System.out.println(obj.getClass());
    }
}
