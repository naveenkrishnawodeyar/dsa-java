package code.finl;

// Immutable class example,
public final class FinalClassDemo
{
    // private variables,
    private int clsId = 01;
    private String clsName = "Boss";
    private boolean clsValue = true;

    // private setters and public getters,
    public int getClsId()
    {
        return clsId;
    }

    private void setClsId(int clsId)
    {
        this.clsId = clsId;
    }

    public String getClsName()
    {
        return clsName;
    }

    private void setClsName(String clsName)
    {
        this.clsName = clsName;
    }

    public boolean getClsValue()
    {
        return clsValue;
    }

    private void setClsValue(boolean clsValue)
    {
        this.clsValue = clsValue;
    }
}
