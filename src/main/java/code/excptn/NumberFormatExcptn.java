package code.excptn;

public class NumberFormatExcptn
{
    static void add(int a, int b)
    {
        System.out.println(a+b);
        String st = "a";
        try
        {
            int s = a+b;
            System.out.println("\nSum of numbers is:"+s+"\n");
            int c = Integer.parseInt(st);
        }
        catch (NumberFormatException e)
        {
            System.out.println("Can't convert from String to int\n"+e);
        }
    }
    public static void main(String[] args)
    {
        System.out.println("\nIf the formatting of any variable or number is mismatched, it may result into NumberFormatException.");
        add(1,2);
    }
}
