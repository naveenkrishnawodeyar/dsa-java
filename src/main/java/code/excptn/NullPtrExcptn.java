package code.excptn;

public class NullPtrExcptn
{
    public static void main(String[] args)
    {
        System.out.println("\nIf we have a null value in any variable, performing any operation on the variable throws a NullPointerException.");
        String s = null;
        try
        {
            System.out.println(s);
            System.out.println(s.toLowerCase());
        }
        catch (NullPointerException e)
        {
            System.out.println("Idiot,Initialize the value\n"+e.getMessage());
        }
    }
}
