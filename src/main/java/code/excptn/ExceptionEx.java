package code.excptn;

public class ExceptionEx
{
    static void test(int a,int b)
    {
        try
        {
            System.out.println(a/b);
        }
        catch (ArithmeticException e)
        {
            System.out.println("\nIdiot, exception occurred");
            System.out.println(e);

            String s = null;
            try
            {
                System.out.println(s);
            }
            catch (NullPointerException n)
            {
                System.out.println("String is empty\n"+n);
            }
        }
    }
    public static void main(String[] args)
    {
        System.out.println("\nException handling is to maintain the normal flow of the application\n");
        test(1,1);
        test(1,0);
    }
    static
    {
        System.out.println("\nan exception is an event that disrupts the normal flow of the program" +
                "It is an object which is thrown at runtime.");
    }
}
