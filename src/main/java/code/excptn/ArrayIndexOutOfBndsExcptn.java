package code.excptn;

public class ArrayIndexOutOfBndsExcptn
{
    static void arryExcptn(int []a)
    {
        try
        {
            System.out.println(a[10]);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println("Check your array size\n"+e);
        }
    }
    public static void main(String[] args)
    {
        System.out.println("When an array exceeds to it's size, the ArrayIndexOutOfBoundsException occurs\n");
        arryExcptn(new int[] {1,7,8,9,4,3,6});
        arryExcptn(new int[] {1,7,8,9,4,3,6,5,4,3,2,1,8});
    }
}
