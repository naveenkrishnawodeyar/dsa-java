package code.excptn.cstmExcptn;

public class ArithMaticEx
{
    static void customException(int age)
    {
        if(age<= 18)
            throw new ArithmeticException("You are kid");
        else
            System.out.println("Can vote");
    }

    public static void main(String[] args)
    {
        System.out.println("We can throw either checked or unchecked exceptions in Java by throw keyword\n" +
                "It is mainly used to throw a custom exception\n");
        customException(17);
    }
}
