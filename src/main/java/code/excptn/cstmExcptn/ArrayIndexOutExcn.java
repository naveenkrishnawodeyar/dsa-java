package code.excptn.cstmExcptn;

public class ArrayIndexOutExcn
{
    static void customException(int[] arr)
    {
        if(arr.length<0)
            throw new ArrayIndexOutOfBoundsException("Array can't be empty");
    }

    public static void main(String[] args)
    {
        System.out.println("We can throw either checked or unchecked exceptions in Java by throw keyword\n" +
                "It is mainly used to throw a custom exception\n");
//        customException(new int[]{5,4,3,2,1});
        System.out.println("\n************\n");
        customException(new int[]{});
    }
}
