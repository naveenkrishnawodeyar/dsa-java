package code.stream;

import java.util.*;
import java.util.stream.Stream;

public class StreamFromColln
{
    public static void main(String[] args)
    {
        System.out.println("**********\n");
//        var intList = new ArrayList<>(10);
        var intList = Arrays.asList(1,2,3,4,5,6,7,8,9);
        System.out.println(intList);
        System.out.println("\n***********\n");
//        Stream<Integer> intStream = intList.stream();
        var intStream = intList.stream();
            intStream.forEachOrdered(System.out::print);
    }
}
