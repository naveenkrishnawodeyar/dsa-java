package code.stream;

import java.util.stream.Stream;

public class StrmCharacterstics
{
    static
    {
        Stream strm = Stream.of(1,2,3,4,5);
        System.out.println(strm);
    }
    public static void main(String[] args)
    {
        System.out.println("\nStream Characteristics\n");
        System.out.println("\n1. Streams are not the data Structures,\n");
        System.out.println("\n2. Stream consumes a datasource,\n");
        System.out.println("\n3. Intermediate and Terminal Operations,\n");
        System.out.println("\n4. Pipeline of operations,\n");
        System.out.println("\n5. Internal iteration,\n");
        System.out.println("\n6. Parallel execution\n");
        System.out.println("\n7. Streams are lazily populated,\n");
        System.out.println("\n8. Streams are lazily populated,\n");
        System.out.println("\n9. Streams are traversable only once,\n");
        System.out.println("\n10. Short circuiting operations,\n");
    }
}
