package code.stream;

import java.util.stream.Stream;

public class SelectionOperations
{
    //1. Stream<T> filter(Predicate<T> predicate,
    static void filteringData(Stream<Integer> strm)
    {
        System.out.println("filter(), is an intermediate operation, which takes predicate FI\n");
        strm.filter(num -> num>=4).forEach(System.out::println);

    }
    public static void main(String[] args)
    {
        System.out.println("************");
        System.out.println("\nStreams can be defined as a sequences of elements from a source which support data processing operations.\n");
        Stream<Integer> intStream = Stream.of(1,2,3,4,5,6,7,8,9);
        filteringData(intStream);
    }
}
