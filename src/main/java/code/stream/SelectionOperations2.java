package code.stream;

import java.util.stream.Stream;

public class SelectionOperations2
{
    //1. distinct() : Selects only unique elements from the datasource,
    static void filteringData(Stream<String> strm)
    {
        strm.distinct().forEach(System.out::println);
    }

    // 2. limit() & skip(), intermediate operations,
    static void limitStream(Stream<String> stream)
    {
        stream.skip(2).limit(3).forEach(System.out::println);
    }
    public static void main(String[] args)
    {
        System.out.println("************");
        System.out.println("\nStreams can be defined as a sequences of elements from a source which support data processing operations.\n");
        Stream<String> intStream = Stream.of("One","1","Two","Three","Four","Five","Two","Three","Four");
        filteringData(intStream);
        System.out.println("************\n");
        limitStream(intStream);
    }
}
