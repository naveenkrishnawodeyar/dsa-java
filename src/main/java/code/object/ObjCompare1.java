package code.object;

public class ObjCompare1
{
    private int objId;
    private String objName;
    private boolean objType;

    // parameterized constructor,
    public ObjCompare1(int a,String b,boolean c)
    {
        this.objId  = a;
        this.objName = b;
        this.objType = c;
    }
    public static void main(String[] args)
    {
        // object creation,
        var obj1 = new ObjCompare1(1,"Nandi",true);
        var obj2 = new ObjCompare1(1,"Nandi",true);

        // object comparison,
        if(obj1.equals(obj2))
            System.out.println("Objects are equal");
        else
            System.out.println("Objects are not equal");
    }

    // overriding the equals() of the object class as for the requirement.,
    @Override
    public boolean equals(Object obj)
    {
        ObjCompare1 obj1 = (ObjCompare1)obj;

        if(this.objId != obj1.objId)
            return false;
        if(!(this.objName.equals(obj1.objName)))
            return false;
        if(this.objType != obj1.objType)
            return false;
        return true;
    }
}
