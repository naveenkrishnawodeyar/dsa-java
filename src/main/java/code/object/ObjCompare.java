package code.object;

public class ObjCompare
{
    private int objId;
    private String objName;
    private boolean objValue;

    public ObjCompare(int objId, String objName, boolean objValue)
    {
        this.objId = objId;
        this.objName = objName;
        this.objValue = objValue;
    }

    @Override
    public boolean equals(Object obj)
    {
        var objCompare = (ObjCompare)obj;
            if(this.objId != objCompare.objId)
                return false;
            if(!(this.objName.equals(objCompare.objName)))
                return false;
            if(this.objValue !=objCompare.objValue)
                return false;
        return true;
    }

    public static void main(String[] args)
    {
        var obj1 = new ObjCompare(1,"Object",true);
        var obj2 = new ObjCompare(1,"Object",true);

            // equals()
            if(obj1.equals(obj2))
                System.out.println("Objects are exactly equal,");
            else
                System.out.println("Objects are not equal,");
    }
}
